# FALCON 9 module landing using nelinear control and PIDs
The semestral project for the Mathematical and Simulation Models course at Faculty of Mechanical engineering CTU.

[Stage 1 landing](media/Landing.png)

## Overview
For the semestral project I chose to build a control model for Falcon9 module landing in 2D. Just the final stage of landing is modeled. The rocket is controlled only with one rocket engine at the bottom of the rocket. The nozzle of this engine has limited tilt angle. There are no helper engines on the side. It means the module has to be relatively close to the platform from horizontal perspective.

The rocket tries to adjust to the landing speed defined in the ParametersAndLogic.m script and shortes path to  [x0,y0] = [0,0] position.
Then it tries to stabilize itself in this position.

## Solution
For the solution look at the see the paper
in /media/SemestralProject.pdf)

Unfortunately, I wrote it in Czech at that time. But the equations should be self-explanatory.

## Try it yourself
After you clone or download the project you can try it simply by running the ParametersAndLogic.m script.
You can also change the initial starting position.

## Example of an output

[Video of a rocket landing](media/MSM.avi)

## Conclusion

For better control in horizontal axis it would be better to have some helper engines on the side near to the top of the rocket.
I would say, that the linearization also creates some troubles for the nozzle. So at some point the nozzle would exceed its angle range. 
