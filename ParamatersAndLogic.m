clc
close all
clear all

rocketWidth = 7; %[m]
rocketHeigth = 60; %[m]

M = 250000;  %[kg]
g = 9.81; %[m.s-2]
%Vertical center of gravity
lt = rocketHeigth/2; 
G = M * g;
u10 = G;
I = M*(20^2+rocketHeigth^2)/12;
%Drag coefficients
cy = 0.5;
cx = 2;
cphi = cy;
lambda = lt /I;

%See the paper for how to get these matrixes
Jx = [0 1 0 0 0 0 
      0 0 0 0 u10 0 
      0 0 0 1 0 0 
      0 0 0 0 0 0 
      0 0 0 0 0 1
      0 0 0 0 lambda*u10 0  ];
 
 Ju = [0 0
       0 u10/M
       0 0
       1/M 0
       0 0
       0 0];
C= [ 1 0 0 0 0 0
     0 0 1 0 0 0
     0 0 0 0 1 0];
 D = [0 0
      0 0
      0 0];
 
CyDrag = 0.5;

v=10;

x0 = 5;
y0 = 200;
phi0 = 0;

vx0 = 0;
vy0 = 0;
vphi0= 0;

ax0 = 0;
ay0 = 0;
aphi0 = 0;


RFk = G;
RFd =G/4;

RAk = 5;
RAd= 5;
simOut = sim('SimulinkModel');
%clear all

%A = imread('Rocket.png');


% widthPic = 5;
% heigthPic = 10;
heigthPlot = y0+100;
widthPlot = heigthPlot;


% actW = widthPlot*widthPic/heigthPlot;
% actH = heigthPlot*heigthPic/widthPlot;

R1 = [-1/2,-1/2,0,1/2,1/2, -1/2] * rocketWidth;
R2=[0,1,1+rocketWidth/rocketHeigth,1,0,0] * rocketHeigth;

T1 = [0,1/2,-1/2,0]; % rocketWidth;
T2 = [0,-2,-2,0]; % rocketWidth;
 

%CAR=plot(P1,P2,'r','Linewidth',5);
 %simTime = 80
 
 length = size(x_out);
 

 Ro1 = zeros(1,6);
 Ro2 = zeros(1,6);
 
 To1 = zeros(1,4);
 To2 = zeros(1,4);
 
 CntR = size(R1);
 CntT = size(T1);
 
 h = figure;
 
 subplot(4, 2, [1,3,5,7]); 
 rocket = fill(R1,R2,'r');
 thrust = fill (T1,T2,'y');
 axis([-widthPlot/2 widthPlot/2 -50 heigthPlot])
 
 %Uncomment this to record the video
 %v = VideoWriter('MSM.avi');
 %open(v);
 
 iFrame = 0;
 tmpl=1 : 1 : length(1);
 nFrame = numel(tmpl);  % [EDITED]
 F(nFrame) = struct('cdata',[],'colormap',[]);
 for n = tmpl
    %hold on
    delete(rocket)
    delete(text)
    delete(thrust)
    clear text
   
    coefRes = force_out(n,2)*4/(1e+06); 
    
    phi_d = phi_out(n,2);
    alfa_d = -alfa_out(n,2);
    
    rMatrix = [cos(phi_d) sin(phi_d); 
                -sin(phi_d) cos(phi_d)];
    tMatrix = [cos(alfa_d) sin(alfa_d); 
                -sin(alfa_d) cos(alfa_d)];

    for k = 1 : CntR(2) 
        TMP = rMatrix * [ R1(1,k); R2(1,k)];
        Ro1(1,k) = TMP(1);
        Ro2(1,k) = TMP(2);
    end
    
    for i = 1 : CntT(2) 
        TMP2 = tMatrix * [ T1(1,i); T2(1,i)];
        To1(1,i) = TMP2(1)*coefRes;
        To2(1,i) = TMP2(2)*coefRes;
    end
    
    t_d = x_out(n,1);
    x_d = x_out(n,2);
    y_d = y_out(n,2);
    F_d = force_out(n,2);
    alfa_d = alfa_out(n,2);
    
    
    Ro1 = Ro1 +x_d;
    Ro2 = Ro2 +y_d;
    
    To1 = To1 +x_d;
    To2 = To2 +y_d;
    subplot(4, 2, [1,3,5,7] ); 
    rocket = fill(Ro1,Ro2,'r');
   
    thrust = fill (To1,To2,'y');
    
    hold on
    trajectory = plot(x_d,y_d,'.','MarkerEdgeColor','k');
    text = text(widthPlot/2-50,heigthPlot-30, num2str(x_out(n)));
    
    axis([-widthPlot/2 widthPlot/2 -50 heigthPlot])
    
    
    subplot(4, 2, 2 ); 
    hold on
    axis([0 80 -5 x0+10])
    plot(t_d,x_d,'.','MarkerEdgeColor','k');
    
    subplot(4, 2, 4 ); 
    hold on
    axis([0 80 -50 y0+20])
    plot(t_d,y_d,'.','MarkerEdgeColor','k');
    
    subplot(4, 2, 6 ); 
    hold on
    axis([0 80 0 5*G])
    plot(t_d,F_d,'.','MarkerEdgeColor','k');
    
    subplot(4, 2, 8 ); 
    hold on
    axis([0 80 -0.5 0.5])
    plot(t_d,alfa_d,'.','MarkerEdgeColor','k');
    
    %pause(0.1)
   % frame_h = get(handle(gcf),'JavaFrame');
   % set(frame_h,'Maximized',1);
    drawnow()
    
    %myFrame = getframe(h);
    %size(myFrame.cdata)
    %writeVideo(v,myFrame.cdata);
    
    iFrame = iFrame + 1;
    F(iFrame) = getframe(h);  % Not i, because this steps in dt
    
    %Uncomment this to record the video
    %writeVideo(v,F(iFrame));
    
    
 end
%Uncomment this to record the video
%close(v);